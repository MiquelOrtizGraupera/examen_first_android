package cat.itb.examenproject1.view

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Toast
import androidx.activity.viewModels
import cat.itb.examenproject1.R
import cat.itb.examenproject1.databinding.ActivityGameBinding
import cat.itb.examenproject1.model.Question
import cat.itb.examenproject1.viewmodel.GameViewModel

class GameActivity : AppCompatActivity() {
    private var questions: MutableList<Question> = mutableListOf(
        Question(R.string.q1, false),
        Question(R.string.q2, true),
        Question(R.string.q3, false),
        Question(R.string.q4, true),
        Question(R.string.q5, true),
        Question(R.string.q6, false),
        Question(R.string.q7, true),
        Question(R.string.q8, false),
        Question(R.string.q9, true),
        Question(R.string.q10, false)
    )

    lateinit var binding: ActivityGameBinding
    val gameViewModel: GameViewModel by viewModels()
    var buttonPushed:Int = 0
    var aciertos:Int = 0
    var fallos:Int = 0

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityGameBinding.inflate(layoutInflater)
        setContentView(binding.root)


        //binding.textView2.text = getString(R.string.q1)
        //binding.textView3.text = getString(R.string.r1)
        showQuestions()
        showRounds()

        binding.button3.setOnClickListener {
            gameViewModel.counterButton()
            println(buttonPushed)
            showQuestions()
            showRounds()
            respostaFalse()
            if(gameViewModel.counterList.value!!.size == 9){
                endGame()
            }
        }
        binding.button4.setOnClickListener {
            gameViewModel.counterButton()
            println(buttonPushed)
            showQuestions()
            showRounds()
            respostaTrue()
            if(gameViewModel.counterList.value!!.size == 9){
                endGame()
            }
        }


    }

    fun showRounds(){
        when(gameViewModel.counterList.value!!.size){
            0 -> binding.textView3.text = getString(R.string.r1)
            1 -> binding.textView3.text = getString(R.string.r2)
            2 -> binding.textView3.text = getString(R.string.r3)
            3 -> binding.textView3.text = getString(R.string.r4)
            4 -> binding.textView3.text = getString(R.string.r5)
            5 -> binding.textView3.text = getString(R.string.r6)
            6 -> binding.textView3.text = getString(R.string.r7)
            7 -> binding.textView3.text = getString(R.string.r8)
            8 -> binding.textView3.text = getString(R.string.r9)
            9 -> binding.textView3.text = getString(R.string.r10)
        }
    }
    fun showQuestions(){
        when(gameViewModel.counterList.value!!.size){
            0 -> binding.textView2.text = getString(R.string.q1)
            1 -> binding.textView2.text = getString(R.string.q2)
            2 -> binding.textView2.text = getString(R.string.q3)
            3 -> binding.textView2.text = getString(R.string.q4)
            4 -> binding.textView2.text = getString(R.string.q5)
            5 -> binding.textView2.text = getString(R.string.q6)
            6 -> binding.textView2.text = getString(R.string.q7)
            7 -> binding.textView2.text = getString(R.string.q8)
            8 -> binding.textView2.text = getString(R.string.q9)
            9 -> binding.textView2.text = getString(R.string.q10)
        }
    }
    fun respostaTrue(){
        if(!questions[gameViewModel.counterList.value!!.size].answer){
            Toast.makeText(this, "Molt bé!!", Toast.LENGTH_SHORT).show()
            aciertos++
        }else{
            Toast.makeText(this,"NO! INCORRECTE", Toast.LENGTH_SHORT).show()
            fallos++
        }
    }
    fun respostaFalse(){
        if(questions[gameViewModel.counterList.value!!.size].answer){
            Toast.makeText(this, "Molt bé!!", Toast.LENGTH_SHORT).show()
            aciertos++
        }else{
            Toast.makeText(this, "NO! INCORRECTE", Toast.LENGTH_SHORT).show()
            fallos++
        }
    }
    fun endGame(){
        val intent = Intent(this, ResultActivity::class.java)
        intent.putExtra("Score", aciertos)
        startActivity(intent)
    }
}