package cat.itb.examenproject1.model

data class Question (val questionText: Int, val answer: Boolean)