package cat.itb.examenproject1.viewmodel

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel

open class GameViewModel: ViewModel(){
    var buttonPushed = 0

    var counterList: MutableLiveData<MutableList<Int>> = MutableLiveData<MutableList<Int>>().apply {
        value = mutableListOf()
    }

    fun counterButton(){
        buttonPushed++
        counterList.value!!.add(buttonPushed)
    }
}